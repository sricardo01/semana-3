﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_binding
{
    class Producto
    {
        //Atributo
        private int id;
        private string nombre;
        private double proteina;
        private string codigo;
        

        //Metodos


        public Producto obtenerProductoPorCodigo(string codigo)
        {
            Producto producto = new Producto();

            string instruccion = "select * from producto where codigo = " + "'" + codigo + "'";
            ManejoSql sql = new ManejoSql();
            // Obtener la coneccion
            SqlConnection conn = sql.obtenerConexion();
            // Abrir conexion
            conn.Open();
            // Interactuar con la base
            SqlCommand comando = new SqlCommand(instruccion, conn);
            SqlDataReader registro = comando.ExecuteReader();
            while(registro.Read())
            {
                producto.Id =  Convert.ToInt32(registro["id"]);
                producto.Nombre = registro["nombre"].ToString();
                producto.Codigo = registro["codigo"].ToString();
                producto.Proteina = Convert.ToDouble(registro["proteina"]);
            }
            // Cerrar la coneccion
            conn.Close();
            return producto;
        }

        public void guardarProducto(Producto producto)
        {

            string proteina = reemplazarString(producto.Proteina.ToString(), ",", ".");

            string instruccion = "";
             if( producto.Id == 0) {
                instruccion = "insert into producto(codigo, nombre, proteina) values('" + producto.Codigo + "','" + producto.Nombre + "'," + proteina + ")";
            }else
            {
                instruccion = "update producto set codigo = '" + producto.Codigo + "', nombre = '" + producto.nombre + "', proteina = " + proteina + " where id = " + producto.id;

            }



            // Obtengo mi conexcion
            ManejoSql sql = new ManejoSql();
            SqlConnection conn = sql.obtenerConexion();
            // Abro mi conexion
            conn.Open();
            // realizo algo en base
            SqlCommand commando = new SqlCommand(instruccion, conn);
            commando.ExecuteNonQuery();
            // cierro mi conexion
            conn.Close();
        }


        /*public void actualizarProducto(Producto producto)
        {

            string proteina = reemplazarString(producto.Proteina.ToString(), ",", ".");
            string instruccion = "update producto set codigo = '" + producto.Codigo + "', nombre = '" + producto.nombre + "', proteina = " + proteina + " where id = " + producto.id;
            // Obtengo mi conexcion
            ManejoSql sql = new ManejoSql();
            SqlConnection conn = sql.obtenerConexion();
            // Abro mi conexion
            conn.Open();
            // realizo algo en base
            SqlCommand commando = new SqlCommand(instruccion, conn);
            commando.ExecuteNonQuery();
            // cierro mi conexion
            conn.Close();
        }*/



        private string reemplazarString(string cadena, string caracterAreemplezar, string reemplazo)
        {
            //cadena --> 2,5
            //caracterAreemplezar ,
            // .
            // 2.5

            return cadena.Replace(caracterAreemplezar, reemplazo);
                
        }


        // Gets and Set

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public double Proteina
        {
            get { return proteina; }
            set { proteina = value; }
        }

    }
}
