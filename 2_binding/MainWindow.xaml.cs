﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _2_binding
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // Vamos a traer de la base de datos
        Producto producto1 = new Producto { Id = 0, Nombre = null , Proteina = 0, Codigo =null };

        // Binding
        // Servidor --> VISTA
        // Servidor <-->Vista



        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = producto1;
        }


        private void consultar(object sender, RoutedEventArgs e)
        {

            if (String.IsNullOrEmpty(producto1.Codigo))
            {
                MessageBox.Show("Ingrese un codigo valido");
            }
            else
            {

                Producto producto = new Producto();
                producto1 = producto.obtenerProductoPorCodigo(producto1.Codigo);
                if (producto1.Id == 0)
                {
                    MessageBox.Show("Producto no encontrado");
                    
                }
                this.DataContext = producto1;


            }

        }

        private void guardar(object sender, RoutedEventArgs e)
        {

            Producto producto = new Producto();

            if (producto1.Id == 0)
            {
                producto.guardarProducto(producto1);
                MessageBox.Show("Producto: " + producto1.Nombre + " guardado!!");
            }
            else
            {
                producto.guardarProducto(producto1);
                MessageBox.Show("Producto: " + producto1.Nombre + " actualizado!!" );
            }
        }


        private void limpiar(object sender, RoutedEventArgs e)
        {
            this.DataContext = new Producto();
        }
    }
}
